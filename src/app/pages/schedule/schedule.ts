import { Component, ViewChild, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController, IonList, IonRouterOutlet, LoadingController, ModalController, ToastController, Config } from '@ionic/angular';

import { ConferenceData } from '../../providers/conference-data';
import { UserData } from '../../providers/user-data';
import { DirectoryPage } from '../directory/directory.page';
import { AngularFirestore } from '@angular/fire/firestore';
import * as firebase  from "firebase"
import { map } from 'rxjs/operators';
import { messageInfo } from '../../interfaces/message';
import { GetDataService } from '../../providers/get-data.service';

@Component({
  selector: 'page-schedule',
  templateUrl: 'schedule.html',
  styleUrls: ['./schedule.scss'],
})
export class SchedulePage implements OnInit {
  // Gets a reference to the list element
  @ViewChild('scheduleList', { static: true }) scheduleList: IonList;

  ios: boolean;
  dayIndex = 0;
  queryText = '';
  segment = 'all';
  excludeTracks: any = [];
  shownSessions: any = [];
  groups: any = [];
  confDate: string;
  showSearchbar: boolean;
  user:any;
  chatsRef:any;
  channels: any = ['Club Tower 1','Villa Campestre','Brazuca'];
  userUid= "7aHTPQ3UswW6QgbP59di"
  data:any;
  chats:messageInfo[];

  constructor(
    public alertCtrl: AlertController,
    public confData: ConferenceData,
    public loadingCtrl: LoadingController,
    public modalCtrl: ModalController,
    public router: Router,
    public routerOutlet: IonRouterOutlet,
    public toastCtrl: ToastController,
    public config: Config,
    public fs:AngularFirestore,
    public getDataService:GetDataService
  ) { 

    this.fs.collection('users').doc(this.userUid).snapshotChanges()
    .subscribe(val => {
      let data = val.payload.data();
      console.log(data);
      
    });
  
    let chatRef = this.fs.collection('chats', ref => ref.orderBy('timestamp')).valueChanges();
    console.log(chatRef);

  
  }
  getChats(){
    
  
  }
  ngOnInit() {
    this.getDataService.getChats("PFddmrWpz5UFLBeGP9CK","building").subscribe( res => {
      this.chats = res
      console.log(res);
    })
   
    this.getDataService.getRoomsNames("PFddmrWpz5UFLBeGP9CK").subscribe( res => {
      this.data = res
      console.log(this.data);
    })
  }

  updateSchedule() {
    // Close any open sliding items when the schedule updates
    if (this.scheduleList) {
      this.scheduleList.closeSlidingItems();
    }

    this.confData.getTimeline(this.dayIndex, this.queryText, this.excludeTracks, this.segment).subscribe((data: any) => {
      this.shownSessions = data.shownSessions;
      this.groups = data.groups;
    });
  }

  async goToDirectory() {
    // const modal = await this.modalCtrl.create({
    //   component: DirectoryPage,
    //   swipeToClose: true,
    //   presentingElement: this.routerOutlet.nativeEl,
    //   componentProps: { excludedTracks: this.excludeTracks }
    // });
    // await modal.present();

    // const { data } = await modal.onWillDismiss();
    // if (data) {
    //   this.excludeTracks = data;
    //   this.updateSchedule();
    // }
    this.router.navigate(['/chats'])
    //  this.router.navigate(['/directory'])

  }

  async addFavorite(slidingItem: HTMLIonItemSlidingElement, sessionData: any) {
    if (this.user.hasFavorite(sessionData.name)) {
      // Prompt to remove favorite
      this.removeFavorite(slidingItem, sessionData, 'Favorite already added');
    } else {
      // Add as a favorite
      this.user.addFavorite(sessionData.name);

      // Close the open item
      slidingItem.close();

      // Create a toast
      const toast = await this.toastCtrl.create({
        header: `${sessionData.name} was successfully added as a favorite.`,
        duration: 3000,
        buttons: [{
          text: 'Close',
          role: 'cancel'
        }]
      });

      // Present the toast at the bottom of the page
      await toast.present();
    }

  }

  async removeFavorite(slidingItem: HTMLIonItemSlidingElement, sessionData: any, title: string) {
    const alert = await this.alertCtrl.create({
      header: title,
      message: 'Would you like to remove this session from your favorites?',
      buttons: [
        {
          text: 'Cancel',
          handler: () => {
            // they clicked the cancel button, do not remove the session
            // close the sliding item and hide the option buttons
            slidingItem.close();
          }
        },
        {
          text: 'Remove',
          handler: () => {
            // they want to remove this session from their favorites
            this.user.removeFavorite(sessionData.name);
            this.updateSchedule();

            // close the sliding item and hide the option buttons
            slidingItem.close();
          }
        }
      ]
    });
    // now present the alert on top of all other content
    await alert.present();
  }

  async openSocial(network: string, fab: HTMLIonFabElement) {
    const loading = await this.loadingCtrl.create({
      message: `Posting to ${network}`,
      duration: (Math.random() * 1000) + 500
    });
    await loading.present();
    await loading.onWillDismiss();
    fab.close();
  }
}
