import { Component, OnInit, Query, ViewChild } from '@angular/core';
// import { AngularFireAuth } from '@angular/fire/auth/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { log } from 'util';
// import { AngularFireAuth } from '@angular/fire/auth/auth';
// import { AngularFirestore } from '@angular/fire/firestore/firestore';
import * as firebase  from "firebase"
import { Timestamp } from 'rxjs/internal/operators/timestamp';
@Component({
  selector: 'app-chats',
  templateUrl: './chats.page.html',
  styleUrls: ['./chats.page.scss'],
})
export class ChatsPage implements OnInit {
  // @ViewChild(Content) content: Content;
  text:string;
  chatRef:any;
  uid:string;
  constructor(public fs:AngularFirestore) {
    // this.uid = localStorage.getItem('userid');
    this.chatRef = this.fs.collection('chats', ref => ref.orderBy('timestamp')).valueChanges();
    console.log(this.chatRef);
    
   }
  //  scrollToBottom() {
  //   setTimeout(() => {
  //     if (this.content.scrollToBottom) {
  //       this.content.scrollToBottom();
  //     }
  //   }, 400)
  // }
  ngOnInit() {
  }

  send(){
    console.log(Date.now());
    if(this.text != ""){
      firebase.firestore().collection("chats").add({
        name: "Daniel",
        message:this.text,
        userId:"123456789",
        timestamp: Date.now()
      });
      this.text = "";
    }
  }


}
