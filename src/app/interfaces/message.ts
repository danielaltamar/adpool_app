export interface messageInfo {
    message: string;
    name: string;
    timestamp: number;
    lastname:string;
    id:string;
  }
  