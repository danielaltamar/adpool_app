import { Injectable } from '@angular/core';
import {messageInfo} from '../interfaces/message'
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AngularFirestore } from '@angular/fire/firestore/';
import { AngularFirestoreCollection } from '@angular/fire/firestore/';

map
@Injectable({
  providedIn: 'root'
})
export class GetDataService {
private chatsCollection:AngularFirestoreCollection<messageInfo>;
private chats: Observable<messageInfo[]>
  constructor(public db: AngularFirestore) { 

  }

  
  getChats(y, x){
    return this.db.collection('chats_test')
    .doc(y)
    .collection('rooms')
    .doc(x)
    .collection<messageInfo>('messages', ref => ref.orderBy('timestamp'))
    .valueChanges();
  }
  getRoomsNames(y){
    return this.db.collection('chats_test')
    .doc(y)
    .collection('rooms').valueChanges();
  }
  getChat(id:string){
    return this.chatsCollection.doc<messageInfo>(id).valueChanges();
  }


}
