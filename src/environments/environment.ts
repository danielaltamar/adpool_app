// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyAni3umJJmNtiCcIv2nKAHDuez9sk_Fzl4",
    authDomain: "adpool-d592d.firebaseapp.com",
    databaseURL: "https://adpool-d592d.firebaseio.com",
    projectId: "adpool-d592d",
    storageBucket: "adpool-d592d.appspot.com",
    messagingSenderId: "631624079574",
    appId: "1:631624079574:web:9b2989606afb63266d1177",
    measurementId: "G-DMLRDKS9E2"
  }
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
